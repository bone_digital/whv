<?php

namespace App;

use Roots\Sage\Container;
use Roots\Sage\Assets\JsonManifest;
use Roots\Sage\Template\Blade;
use Roots\Sage\Template\BladeProvider;

use App\Bone\CookieNotice;
use App\Bone\Admin\Maintenance;
use App\Bone\Security;
use App\Bone\Theme;
use App\Bone\Emails;
use App\Bone\Admin\ACF;

new ACF();
new Theme();
new Security();
new Emails();
new Maintenance();
new Ajax();
new CookieNotice();

/**
 * Setup Image Sizes
 *
 * add_image_size($name, $width, $height, $crop)
 */
//add_image_size('image_size_name', 300, 300, false);
add_image_size('card', 1200, 1200, false);

/**
 * Disable Gutenberg
 */
add_filter('use_block_editor_for_post', '__return_false');

/**
 * Remove Default Image Sizes
 */
function remove_default_image_sizes( $sizes )
{
	// Keep the thumbnail size for the media library
	// unset( $sizes['thumbnail']);
	unset( $sizes['medium']);
	unset( $sizes['medium_large']);
	unset( $sizes['large']);

	return $sizes;
}
add_filter('intermediate_image_sizes_advanced', 'App\remove_default_image_sizes');

/**
 * Theme assets
 */
add_action('wp_enqueue_scripts', function () {
    $version = null;
    $environment = env('WP_ENV');

    //Use the date/time as the version number on staging and development environments to avoid caching
    if('production' != $environment)
    {
        $version = date('YmdHis');
    }

    wp_enqueue_style('sage/main.css', asset_path('styles/main.css'), false, $version);
    wp_enqueue_script('sage/main.js', asset_path('scripts/main.js'), ['jquery'], $version, true);
	wp_localize_script('sage/main.js', 'Ajax', [
		'url'	=> admin_url('admin-ajax.php')
	]);
    wp_localize_script('sage/main.js', 'Entry', [
        'entry' => \App::gfConfirmation()
	]);

    if (is_single() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
	}

	//Add in Gravity Forms
	gravity_form_enqueue_scripts( 1, true );
}, 100);

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable features from Soil when plugin is activated
     * @link https://roots.io/plugins/soil/
     */
    add_theme_support('soil-clean-up');
    add_theme_support('soil-jquery-cdn');
    add_theme_support('soil-nav-walker');
    add_theme_support('soil-nice-search');
    add_theme_support('soil-relative-urls');

    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');

    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
		'primary_navigation' => __('Primary Navigation', 'sage'),
		'footer_1_navigation' => __('Footer One Navigation', 'sage'),
		'footer_2_navigation' => __('Footer Two Navigation', 'sage')
    ]);

    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');

    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);

    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    add_theme_support('customize-selective-refresh-widgets');

    /**
     * Use main stylesheet for visual editor
     * @see resources/assets/styles/layouts/_tinymce.scss
     */
    add_editor_style(asset_path('styles/main.css'));
}, 20);

/**
 * Register sidebars
 */
add_action('widgets_init', function () {
    $config = [
        'before_widget' => '<section class="widget %1$s %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>'
    ];
    register_sidebar([
        'name'          => __('Primary', 'sage'),
        'id'            => 'sidebar-primary'
    ] + $config);
    register_sidebar([
        'name'          => __('Footer', 'sage'),
        'id'            => 'sidebar-footer'
    ] + $config);
});

/**
 * Updates the `$post` variable on each iteration of the loop.
 * Note: updated value is only available for subsequently loaded views, such as partials
 */
add_action('the_post', function ($post) {
    sage('blade')->share('post', $post);
});

/**
 * Provide the editor with access to manage the menus
 */
add_action( 'after_switch_theme', function() {
	$role = get_role('editor');
	$role->add_cap( 'gform_full_access' );
	$role->add_cap( 'edit_theme_options' );
	$role->add_cap( 'manage_options' );
});

/**
 * Setup ACF Pro Key
 */
add_action( 'after_switch_theme', function() {
	if ( ! get_option( 'acf_pro_license' ) && defined( 'ACF_PRO_KEY' ) )
	{
		$save = array(
			'key'	=> ACF_PRO_KEY,
			'url'	=> home_url()
		);

		$save = maybe_serialize($save);
		$save = base64_encode($save);

		update_option( 'acf_pro_license', $save );
	}
});

/**
 * Setup Sage options
 */
add_action('after_setup_theme', function () {
    /**
     * Add JsonManifest to Sage container
     */
    sage()->singleton('sage.assets', function () {
        return new JsonManifest(config('assets.manifest'), config('assets.uri'));
    });

    /**
     * Add Blade to Sage container
     */
    sage()->singleton('sage.blade', function (Container $app) {
        $cachePath = config('view.compiled');
        if (!file_exists($cachePath)) {
            wp_mkdir_p($cachePath);
        }
        (new BladeProvider($app))->register();
        return new Blade($app['view']);
    });

    /**
     * Create @asset() Blade directive
     */
    sage('blade')->compiler()->directive('asset', function ($asset) {
        return "<?= " . __NAMESPACE__ . "\\asset_path({$asset}); ?>";
    });
});

/**
 * Disables the password changed email to the administrator
 */
if( function_exists( 'get_field' ) )
{
	if( get_field('disable_send_password_change_notifications', 'options') )
	{
		if( !function_exists( 'wp_password_change_notification' ) )
		{
			function wp_password_change_notification($user)
			{
				return;
			}
		}
	}
}
