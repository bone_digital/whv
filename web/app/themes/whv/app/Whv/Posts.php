<?php

namespace App\Whv;

class Posts
{
	public static function GetPostsPageId()
	{
		return get_option('page_for_posts');
	}
	
	public static function GetPosts($paged = null, $query_only = false, $category = null)
	{
		if( null == $paged )
		{
			$paged = 1;
		}
		
		$args = [
			'post_type'	=> 'post',
			'posts_status'	=> 'publish',
			'paged'	=> $paged,
			'orderby'	=> 'date',
			'order'	=> 'desc',
		];

		if (get_query_var( 'cat' )) {
			$category = get_category( get_query_var( 'cat' ) );
			$args['cat'] = $category->cat_ID;
		}
		
		$query = new \WP_Query($args);
		
		if( true == $query_only )
		{
			return $query;
		}
		
		$posts = false;
		if( $query->have_posts() )
		{
			$posts = [];
			while($query->have_posts())
			{
				$query->the_post();
				$posts[] = self::GetCardDetails(get_the_ID());
			}
		}
		
		wp_reset_query();
		return $posts;
	}
	
	public static function GetFeaturedPosts()
	{
		$post_ids = null;
		$posts_page_id = self::GetPostsPageId();
		$featured_post_ids = get_field('featured_posts', $posts_page_id);
		if( !empty($featured_post_ids) && is_array($featured_post_ids) )
		{
			$post_ids = [];
			$counter = 0;
			foreach($featured_post_ids as $post_id)
			{
				$post_ids[] = $post_id;
				$counter++;
				if( $counter > 2 )
				{
					break;
				}
			}
		}
		return $post_ids;
	}
	
	public static function GetCardDetails( $post_id = null )
	{
		if( null == $post_id )
		{
			$post_id = get_the_ID();
		}
		
		$details = [
			'title'	=> get_the_title($post_id),
			'link'	=> get_the_permalink($post_id),
			'image_url'	=> self::GetFeaturedImage($post_id),
			'sub_title'	=> get_field('sub_title', $post_id),
			'category'	=> self::GetPostCategory($post_id),
		];
		
		return $details;
	}
	
	public static function GetFeaturedImage($post_id = null)
	{
		if( null == $post_id )
		{
			$post_id = get_the_ID();
		}
		
		$image_id = get_post_thumbnail_id($post_id);
		$image_url = false;
		if( !empty($image_id) )
		{
			$image = wp_get_attachment_image_src($image_id, 'card');
			if( !empty($image) && is_array($image) )
			{
				$image_url = $image[0];
			}
		}
		return $image_url;
	}
	
	public static function GetPostCategory($post_id = null)
	{
		if( null == $post_id )
		{
			$post_id = get_the_ID();
		}
		
		$categories = wp_get_post_terms($post_id, 'category');
		$category = null;
		if( !empty($categories) && !is_wp_error($categories) )
		{
			foreach($categories as $cat)
			{
				$category = $cat->name;
			}
		}
		
		return $category;
	}
}
