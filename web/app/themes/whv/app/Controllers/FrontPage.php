<?php

namespace App\Controllers;

use App\Bone\PageBuilder;
use Sober\Controller\Controller;

class FrontPage extends Controller
{
	public function Layouts()
	{
		//Using Sections
		$page_builder = new PageBuilder(get_field('sections'), true );

		//Using Modules
		//$page_builder = new PageBuilder(get_field('modules'), false );

		return $page_builder->layouts();
	}
}
