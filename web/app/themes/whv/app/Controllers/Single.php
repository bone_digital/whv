<?php

namespace App\Controllers;

use App\Bone\PageBuilder;
use App\Whv\Posts;
use Sober\Controller\Controller;

class Single extends Controller
{
	public function FeaturedImage()
	{
		return Posts::GetFeaturedImage(get_the_ID());
	}
	
	public function SubTitle()
	{
		return get_field('sub_title');
	}

	public function BackUrl()
	{
		$page_id = get_option('page_for_posts');
		$url = get_the_permalink($page_id);
		return $url;
	}

	public function Category()
	{
		$categories = wp_get_post_terms(get_the_ID(), 'category');

		$html = '';
		if( !empty($categories) && !is_wp_error($categories) )
		{
			foreach($categories as $cat)
			{
				$html = $cat->name;
			}
		}
		return $html;
	}

	public function Title()
	{
		return get_the_title();
	}

	public function PostLink()
	{
		return get_the_permalink();
	}

	public function FullArticleLink()
	{
		$link = get_field('full_article_link');
		$url = '';
		if( !empty($link) && is_array($link) )
		{
			$url = $link['url'];
		}
		return $url;
	}

	public function FullArticleTitle()
	{
		$link = get_field('full_article_link');
		$title = '';
		if( !empty($link) && is_array($link) )
		{
			$title = $link['title'];
		}
		return $title;
	}

	public function SupportingLink()
	{
		$link = get_field('supporting_website_link');
		$url = '';
		if( !empty($link) )
		{
			$url = $link['url'];
		}
		return $url;
	}

	public function SupportingLinkTitle()
	{
		$link = get_field('supporting_website_link');
		$title = '';
		if( !empty($link) )
		{
			$title = $link['title'];
			if ( empty($title) && array_key_exists('url', $link) )
			{
				$title = $link['url'];
			}
		}
		return $title;
	}

}
