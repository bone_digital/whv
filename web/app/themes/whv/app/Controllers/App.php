<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class App extends Controller
{
	public function ShowEquals()
	{
		$show = get_field('show_equals', 'options');
		return $show;
	}

	public static function GetPostId()
	{
		$post_id = get_the_ID();


		if( is_archive() || is_home() )
		{
			$post_id = get_option('page_for_posts');
		}
		return $post_id;
	}

	public function HeaderImage()
	{
		$post_id = self::GetPostId();
		$header_type = $this->HeaderType();
		$header_image_url = '';
		if( 'page-header--default' == $header_type )
		{
			$image = get_field('header_image', $post_id);
			$header_image_url = App::getImageUrlFromField($image, 'card');
		}
		return $header_image_url;
	}

	public function HeaderSlides()
	{
		wp_reset_query();
		$post_id = self::GetPostId();
		return get_field('slides', $post_id);
	}

	public function HeaderClasses()
	{
		$post_id = self::GetPostId();
		$header_type = $this->HeaderType();
		$classes = $header_type;
		if( 'page-header--default' == $header_type )
		{
			$classes .= ' ' . get_field('header_colour', $post_id);
			if( !empty($this->HeaderImage()) )
			{
				$classes .= ' page-header--has-image';
			}
		}
		return $classes;
	}

	public function ShowHeader()
	{
		if( !is_singular('post') && !is_single() )
		{
			return true;
		}
		return false;
	}

	public function HeaderContentShadow(){
		$post_id = self::GetPostId();
		if(false == $this->ShowHeader())
		{
			return false;
		}

		$shadow_color = get_field('header_content_shadow', $post_id);
		return $shadow_color;
	}

	public function HasBoldFont(){
		$post_id = self::GetPostId();
		if(false == $this->ShowHeader())
		{
			return false;
		}

		$has_bold_font = get_field('has_bold_font', $post_id);
		return $has_bold_font;
	}
	
	public function HeaderContent()
	{
		$post_id = self::GetPostId();
		if(false == $this->ShowHeader())
		{
			return false;
		}

		$content = get_field('header_content', $post_id);
		return $content;
	}

	public function HeaderType()
	{
		$post_id = self::GetPostId();
		if(false == $this->ShowHeader())
		{
			return false;
		}

		$header_type = get_field('header_type', $post_id);
		return $header_type;
	}

	public function ShowFooterCallToAction()
	{
		if( true == get_field('footer_hide_call_to_action') )
		{
			return false;
		}
		else if (is_home() || is_singular('post'))
		{
			return false;
		}

		return true;
	}

	/**
	 * Get the Section Class
	 *
	 */
	public function sectionClass()
	{
		$post_id = get_the_ID();
		return get_field('section_class', $post_id);
	}
	/**
	 * Returns the image title tag from an ID or image field in ACF
	 *
	 * @param $image
	 * @return mixed|string
	 */

	function FooterContent()
	{
		return get_field('footer_content', 'options');
	}

	function FacebookLink()
	{
		return get_field('facebook_link', 'options');
	}

	function TwitterLink()
	{
		return get_field('twitter_link', 'options');
	}

	function InstagramLink()
	{
		return get_field('instagram_link', 'options');
	}

	function YoutubeLink()
	{
		return get_field('youtube_link', 'options');
	}

	function LinkedinLink()
	{
		return get_field('linkedin_link', 'options');
	}

	function FooterCta()
	{
		return get_field('footer_cta', 'options');
	}

	public static function getImageTitle($image)
	{
		$title = '';
		if( null == $image )
		{
			//Empty
			return '';
		}

		if( is_array($image) )
		{
			if( array_key_exists('title', $image) )
			{
				$title = $image['title'];
			}
		}
		else if( is_integer($image) )
		{
			$title = get_the_title($image);
		}

		return $title;
	}

	/**
	 * Returns the image alt tag from an image ID or image field in ACF
	 *
	 * @param $image
	 * @return mixed|string
	 */
	public static function getImageAlt($image)
	{
		$alt = '';
		if( null == $image )
		{
			//Empty
			return '';
		}

		if( is_array($image) )
		{
			if( array_key_exists('alt', $image) )
			{
				$alt = $image['alt'];
			}
		}
		else if( is_integer($image) )
		{
			$alt = get_post_meta( $image, '_wp_attachment_image_alt', true );
		}

		return $alt;
	}

	/**
	 * Generates an img tag with all attributes required
	 * @param $image
	 * @param string $size
	 * @param string $classes
	 * @return string
	 */
	public static function generateImgTag($image, $size = 'full', $classes = '')
	{
		$img_html = '';
		if( !empty($image) )
		{
			$image = $image['ID'];
			$title = App::getImageTitle($image);
			$alt = App::getImageAlt($image);
			$image_url = App::getImageUrlFromField($image, $size);
			$img_html = '<img class="' . $classes . '" title="'. $title . '" alt="' . $alt . '" src="' . $image_url . '" />';
		}
		return $img_html;
	}

	/**
	 * Returns a url from a field or attachment ID
	 *
	 * @param $image ID or image field
	 * @param string $size size of the image
	 * @return mixed|string|void image url or null if can't be found
	 */
	public static function getImageUrlFromField($image, $size = 'full')
	{
		$image_url = '';
		if( null == $image || empty($image) )
		{
			//Doesn't exist
			return;
		}

		if( is_array($image) && !empty($image) )
		{
			if( 'full' == $size )
			{
				$image_url = $image['url'];
			}
			else
			{
				$image_url = $image['sizes'][$size];
			}
		}
		else if( is_integer($image) )
		{
			$obj = wp_get_attachment_image_src($image, $size);
			if( !is_wp_error($obj) )
			{
				$image_url = $obj[0];
			}
		}

		return $image_url;
	}

	/**
	 * Returns the site name
	 *
	 * @return string|void
	 */
    public function siteName()
    {
        return get_bloginfo('name');
    }

	/**
	 * Returns the title based on teh type of page or archive
	 *
	 * @return string|void
	 */
    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

	/**
	 * Outputs the HTML for GA
	 *
	 * @return string|string[]
	 */
    public static function googleAnalytics()
    {
        //Get the UA Code
        $ua_code = get_field('ga_code', 'options');
        $html = '';
        if( !empty($ua_code) )
        {
            $html = "<!-- Global site tag (gtag.js) - Google Analytics -->
                    <script async src=\"https://www.googletagmanager.com/gtag/js?id={{CODE}}\"></script>
                    <script>
                      window.dataLayer = window.dataLayer || [];
                      function gtag(){dataLayer.push(arguments);}
                      gtag('js', new Date());

                      gtag('config', '{{CODE}}');
                    </script>
                    ";
            $html = str_replace('{{CODE}}', $ua_code, $html);
        }
        return $html;
    }

		public static function request($string)
		{
			return (isset($_GET[$string])) ? sanitize_text_field($_GET[$string]) : null;
		}
	
		/**
	 	* The logic to update the confirmation page to display messages based on form answers
	 	*/
		public static function gfConfirmation()
		{
			$entryId = App::request('entryID');
			$result = \GFAPI::entry_exists($entryId);
			$fieldMapping = array();

			if ($result) {
				$entry = \GFAPI::get_entry($entryId);
				if (is_array($entry)) {
					$formId = $entry['form_id'];
					foreach ($entry as $key => $value) {
						if (is_int($key)) {
								$field = \GFAPI::get_field($formId, $key);
								$label = $field->label;
								$choices = $field->choices;
								if ($label && $value && !empty($choices)) {
									$fieldMapping[$label] = $value;
								}
						}
					}
				}
			}
			return $fieldMapping;
		}
}
