<?php

namespace App;

use App\Whv\Posts;
use function App\template;

class Ajax
{
	public function __construct()
	{
		//add_action( 'wp_ajax_function_name', [$this, ajaxExample] );
		//add_action( 'wp_ajax_no_priv_function_name', [$this, 'ajaxExample'] );
		
		add_action( 'wp_ajax_load_blog_page', [$this, 'loadBlogPage'] );
		add_action( 'wp_ajax_nopriv_load_blog_page', [$this, 'loadBlogPage'] );
	}

	
	public function loadBlogPage()
	{
		$page = 1;
		if( isset($_POST['page']) )
		{
			$page = (int)$_POST['page'];
		}
		$posts = Posts::GetPosts($page);
		$query = Posts::GetPosts($page, true);
		foreach($posts as $post)
		{
			echo template('cards.card_blog', $post);
		}
		$remainder = $query->post_count % 3;
		if( $remainder > 0 )
		{
			for($i=0; $i<=$remainder; $i++)
			{
				echo '<div class="empty-div card-blog"></div>';
			}
		}
		
		//End ajax call
		die();
	}
	
	/**
	 * Exanple Ajax call
	 */
	public function ajaxExample()
	{
		//End ajax call
		die();
	}
}
