import $ from 'jquery';
import { update_menu } from '../modules/menu';
import { sliders } from '../modules/sliders';
import { active_section } from '../modules/waypoints';
import { rerun_gravity_forms_scripts, load_more_description, gf_confirmation_logic, block_question_content } from '../modules/forms';
import { setup_canvas } from '../modules/canvas';
import { setup_load_more } from '../modules/blog';
import Rellax from 'rellax';
import 'lity';

export default {
	init()
	{
		// JavaScript to be fired on all pages
		update_menu();
		setup_scrolling();
		setup_back_to_top();
		sliders();
		active_section();
		rerun_gravity_forms_scripts();
		load_more_description();
		gf_confirmation_logic();
		block_question_content();
		setup_video_wrapper();
		setup_canvas();
		setup_load_more();
		parallax();
	},
	finalize()
	{
		// JavaScript to be fired on all pages, after page specific JS is fired
	},
};

/**
 * Parallax some SVG elements
 */
function parallax()
{
	if (document.querySelector('.rellax'))
	{
		var rellax = new Rellax('.rellax', {
			center: false,
		});
	}
}

function setup_video_wrapper()
{
	$('iframe').each(function ()
	{
		let src = $(this).attr('src');
		if (src.includes('youtube') || src.includes('vimeo'))
		{
			$(this).wrap('<div class="video-wrapper"></div>');
		}
	});
}

/**
 * Sets up the scrolled class
 */
function setup_scrolling()
{
	$(window).on('scroll', function ()
	{
		//Add the 'scrolled' class
		let scroll = $(this).scrollTop();
		let body = $('body');
		if (scroll > 0)
		{
			body.addClass('scrolled');
		}
		else
		{
			body.removeClass('scrolled');
		}
	});
}

/**
 * Sets up a scroll to top on any back to top classes
 */
function setup_back_to_top()
{
	$('.back-to-top').click(function (e)
	{
		e.preventDefault();
		$('html,body').animate({
			scrollTop: 0,
		}, 'slow');
	});

	$('a.page-header-full__scroll').click(function (e)
	{
		e.preventDefault();
		let target = $('.section').first().outerHeight() + 27;
		console.log(target);
		$('body,html').animate({
			scrollTop: target,
		}, 'slow');
	});
}
