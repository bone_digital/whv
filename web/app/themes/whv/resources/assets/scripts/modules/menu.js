/**
 * Sets up a click event
 */
export function setup_menu_trigger()
{
	$('.menu-trigger').click(function (e){
		e.preventDefault();
		let body = $('body');
		if(body.hasClass('menu-active'))
		{
			body.removeClass('menu-active');
		}
		else
		{
			body.addClass('menu-active');
		}
	});
}

/**
 * Updates the menu based on the current url
 */
export function update_menu()
{
	//Update the menu aria-current page based on urls
	let url = window.location.href;

	//Remove the old current page attributes
	let current_page = $('.nav li.current-menu-item');
	current_page.removeClass('current-menu-item').find('a').attr('aria-current', '');

	//Find the new current page and add classes
	let active_items = $('.nav').find('a[href="' + url + '"]');
	active_items.each(function(){
		$(this).attr('aria-current', 'page');
		$(this).parent().addClass('current-menu-item');
	});
}
