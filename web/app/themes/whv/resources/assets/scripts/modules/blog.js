import { active_section } from './waypoints';

export function setup_load_more()
{
	$('a.blog-posts__load-more').click((e) => {
		e.preventDefault();
		let page = Number($('input.current_page').val());
		let maxPages = Number($('input.max_num_pages').val());
		page += 1;
		if( page > maxPages )
		{
			//Can't proceed
			return;
		}
		let body = $('body');
		if( body.hasClass('loading') )
		{
			return;
		}

		body.addClass(('loading'));
		let data = {
			action: 'load_blog_page',
			page: page,
		};
		console.log(maxPages);
		console.log(page);
		if( page === maxPages )
		{
			$('a.blog-posts__load-more').fadeOut('fast');
		}
		$('input.current_page').val(page);

		$.post(Ajax.url, data, function(res){
			$('.blog-posts__inner').append(res);
			body.removeClass('loading');
			active_section();
		})

	});
}
