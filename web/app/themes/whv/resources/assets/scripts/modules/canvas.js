import $ from 'jquery';

let images = [];
export function setup_canvas()
{
	$(window).resize(function(){
		//Setup the canvas
		let canvas = document.getElementById('custom-cursor__canvas');
		if($('#custom-cursor__canvas').length <= 0)
		{
			return;
		}

		let canvas_wrapper = $('.custom-cursor');
		canvas.width = canvas_wrapper.width();
		canvas.height = canvas_wrapper.height();
	})

	//Setup the canvas
	let canvas = document.getElementById('custom-cursor__canvas');
	if($('#custom-cursor__canvas').length <= 0)
	{
		return;
	}
	let canvas_wrapper = $('.custom-cursor');

	//Preload the images
	let imageString = canvas_wrapper.attr('data-images');
	if(imageString.length <= 0)
	{
		return;
	}
	imageString = imageString.slice(0, -1);
	//Reset images for page load via swup
	images = [];
	let imageUrls = imageString.split(',');
	imageUrls.forEach(function(url){
		let image = new Image();
		image.src = url;
		images.push(image);
	});

	//Set the width / height
	canvas.width = canvas_wrapper.width();
	canvas.height = canvas_wrapper.height();

	//Get the context
	let ctx = canvas.getContext('2d');
	let click_overwrite = false;

	if( isTouch() )
	{
		canvas.onclick = function (event) {
			let index = getImageIndex();
			let img = images[index];
			click_overwrite = true;

			//Get the x y
			let x = event.offsetX - img.width / 2;
			let y = event.offsetY - img.height / 2;

			ctx.drawImage(img, x, y);
		};
	}

	//Manage the event
	canvas.onmousemove = function(event){
		let index = getImageIndex();
		let img = images[index];

		if(true == click_overwrite )
		{
			return;
		}

		//Get the x y
		let x = event.offsetX;
		let y = event.offsetY;

		if(showImage(x, y))
		{
			ctx.drawImage(img, x, y);
		}
	};
}

let selectedIndex = 0;
function getImageIndex()
{
	selectedIndex++;
	if( selectedIndex > images.length - 1)
	{
		selectedIndex = 0;
	}
	return selectedIndex;
}

let lastX = 0;
let lastY = 0;
let tolerance = 100;

function showImage(x, y)
{
	if (Math.abs(lastX - x) > tolerance) {
		lastX = x;
		lastY = y;
		return true;
	}

	if (Math.abs(lastY - y) > tolerance) {
		lastX = x;
		lastY = y;
		return true;
	}

	return false;
}

function isTouch()
{
	if ('ontouchstart' in window || window.TouchEvent)
	{
		return true;
	}
	return false;
}
