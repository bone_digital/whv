import 'owl.carousel';

export function sliders()
{
	let sliders = $('.module-logo__slider');

	sliders.each(function()
	{
		//Change the options here - you can use data-* tags to specify different values for different sliders


		let options = {
			items: 5,
			margin: 50,
			loop: true,
			dots: true,
			autoplay: false,
			responsive: {
				0: {
					items: 1,
				},
				700: {
					items: 3,
				},
				900: {
					items: 5,
				},
			},
		};

		//Turn autoplay on when there are more than 5 slides
		if( $(this).find('.module-logo__slide').length > 5 )
		{
			options.autoplay = true;
			options.autoplayTimeout = 3000;
		}


		$(this).owlCarousel(options);
	});

	sliders = $('.page-header-full__slider');

	sliders.each(function()
	{
		//Change the options here - you can use data-* tags to specify different values for different sliders
		let options = {
			items: 1,
			smartSpeed: 1000,
			autoplayTimeout: 5000,
			loop: false,
			autoplay: true,
		};

		$(this).owlCarousel(options);
	});

	//Add additional code here..
}
