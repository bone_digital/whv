/*global renderRecaptcha*/
var onloadCallback = function () {
	renderRecaptcha();
}

export function rerun_gravity_forms_scripts()
{
	//Only find scripts inside the app-container as they are within Swup
	let selector = $('.body-wrapper').find('.gform_wrapper');

	if(selector.length > 0)
	{
		setTimeout(function () {
			let recaptcha_wrap = $('.ginput_recaptcha');
			let recaptcha_iframe = recaptcha_wrap.find('iframe');
			let og_script = 'https://www.google.com/recaptcha/api.js?hl=en&render=explicit&ver=5.2.2';
			let new_script = 'https://www.google.com/recaptcha/api.js?onload=onloadCallback&hl=en&render=explicit&ver=5.2.2';

			if (recaptcha_iframe.length <= 0 && recaptcha_wrap.length > 0) {
				if ($('script[src="' + og_script + '"]').length <= 0 || $('script[src="' + new_script + '"]').length <= 0) {
					$('body').append('<script type="text/javascript" src="' + new_script + '"></script>');
				}

				$(document).on('gform_post_render', function () {
					renderRecaptcha();
				});

			}

		}, 250);

		selector.each(function(){
			let scripts = $(this).parent().find('script');
			scripts.each(function(){
				let text = $(this).text();
				eval(text);
			});
		});
	}
}

export function load_more_description()
{
	let parent = $('.marlin-custom-form');
	if (parent.length) {
		parent.find('.gfield').each(function(key, elem) {
			let gfield_desc = $(elem).find('.gfield_description');
			if (gfield_desc.length) {
				gfield_desc.hide();
				$('<a class="learn-more-link" herf="#">Learn more</a>').insertBefore(gfield_desc);
			}

			if ($(elem).hasClass('requirements-block')) {
				$(elem).hide();
				$('<a class="more-requirements-link" herf="#">Requirements for achieving this</a>').insertBefore($(elem));
			}
		});

		$('.learn-more-link').on('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('open').parent().find('.gfield_description').stop().slideToggle();
		});

		$('.more-requirements-link').on('click', function(e) {
			e.preventDefault();
			$(this).toggleClass('open').next('.requirements-block').stop().slideToggle();
		});
	}
}

export function gf_confirmation_logic()
{
	let count = 1;
	let reverse = 9999;
	if (window.Entry.entry) {
		let entry = window.Entry.entry;
		const entries = Object.entries(entry);
		$('#body-wrapper').addClass('workforce-development-answers');
		entries.forEach((element) => {
			if (element[1] == 'Yes') { 
				$(':contains(' + element[0] + ')').parents('.section').css('order', reverse--);
			} else if(element[1] == 'No') {
				$(':contains(' + element[0] + ')').parents('.section').css('order', count++);
			}
		});

		if (count == 1) {
			$('.fail-heading').css('display', 'none');
		}

		if ($('.success-heading').length && reverse != 9999) {
			$('.success-heading').css('order', reverse--);
		} else {
			$('.success-heading').css('display', 'none');
		}
	}
}

export function block_question_content()
{
	$('.content-question').on('change', function() {
		$(this).parents('.module-one_column_question__inner').find('.load-more-content').stop().slideToggle();
	})
}
