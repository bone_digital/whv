// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'

// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';

// Additional Libraries
import { setup_menu_trigger } from './modules/menu';
import { setup_cookie_notice_popup } from './modules/cookie-notice';

// Swup setup
import Swup from 'swup';
import SwupBodyClassPlugin from '@swup/body-class-plugin';
import SwupScrollPlugin from '@swup/scroll-plugin';

const options = {
	linkSelector: `a[href^="${window.location.origin}"]:not([data-no-swup]):not([target="_blank"]), a[href^="/"]:not([data-no-swup]):not([target="_blank"]), a[href^="#"]:not([data-no-swup]):not([target="_blank"])`,
	plugins: [
		new SwupBodyClassPlugin(),
		new SwupScrollPlugin({
			doScrollingRightAway: false,
			animateScroll: false,
		}),
	],
	animateHistoryBrowsing: true,
	containers: [
		'#body-wrapper',
		'#footer-cta-wrapper',
	],
};
const swup = new Swup(options);

/** Populate Router instance with DOM routes */
const routes = new Router({
	// All pages
	common,
	// Home page
	home,
});

// Generic events - these are loaded once only and would not re-run on Swup page changes
jQuery(document).ready(() => {
	// Place one off code here - such as click events for the menu
	setup_menu_trigger();
	setup_cookie_notice_popup();
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

// Load Events when using Swup
swup.on('contentReplaced', () => routes.loadEvents());
