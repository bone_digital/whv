<div class="card-blog" data-wp>

	<div class="card-blog__inner">

		<a href="{!! $link !!}" class="card-blog__image" style="background: url({!! $image_url !!}) no-repeat center; background-size: cover;"></a>

		<div class="card-blog__category">{!! $category !!}</div>

		<a href="{!! $link !!}" class="card-blog__title content-button content-button--block"><p>{!! $title !!}</p></a>

		<div class="card-blog__excerpt">
			{!! $sub_title !!}
		</div>

	</div>

</div>
