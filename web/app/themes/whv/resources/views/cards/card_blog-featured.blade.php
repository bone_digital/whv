<div class="card-featured" data-wp>

	<div class="card-featured__inner">

		<div class="card-featured__left">

			<div class="card-featured__top">

				<div class="card-featured__category"><p>{!! $category !!}</p></div>

				<a class="card-featured__title" href="{!! $link !!}"><h2>{!! $title !!}</h2></a>

			</div>

			<div class='card-featured__bottom'>

				@if( $sub_title)
				<div class='card-featured__sub'><p>{!! $sub_title !!}</p></div>
				@endif

				<a class='content-button content-button--block' href="{!! $link !!}">Keep reading</a>

			</div>

		</div>

		<a href="{!! $link !!}" class="card-featured__right" style="background: url({!! $image_url !!}) no-repeat center; background-size: cover;"></a>

	</div>

</div>
