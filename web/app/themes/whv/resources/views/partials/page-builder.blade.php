@if( !empty($layouts) && is_array($layouts) )
	
	@if($layouts['type'] == 'sections')
		@foreach($layouts['sections'] as $section)
			@include('sections.' . $section['name'], $section['data'])
		@endforeach
	@elseif($layouts['type'] == 'modules')
		@foreach($layouts['modules'] as $module)
			@include('modules.' . $module['name'], $module['data'])
		@endforeach
	@endif

@endif
