@if($show_header)
@if('page-header--default' == $header_type)
	<div class="page-header-default {!! $header_classes !!}"
		@if($header_image)
		style="background: url({!! $header_image !!}) no-repeat center; background-size: cover;"
		@endif
	>
		<div class='inner-wrapper'>

			<div class="page-header-default__inner">

				@if($header_content && $header_content_shadow)
					<style>
						.page-header-default h1{
							box-shadow: inset 0px -40px 0 0px {!! $header_content_shadow !!};
							display: inline;
							line-height: 1.3;
						}
						@media only screen and (max-width: 700px){
							.page-header-default h1{
								box-shadow: inset 0px -25px 0 0px {!! $header_content_shadow !!};
							}
						}
					</style>
				@endif
				@if($header_content)
					<h1 class="page-header-heading"
					@if($has_bold_font)
						style="font-weight:bold;"
					@endif
				>{!! $header_content !!}</h1>
				@endif
			</div>

		</div>

	</div>

@else
	@if ($header_slides)
	<div class="page-header-full">

		<div class="page-header-full__inner">

			<div class="page-header-full__slider owl-carousel">

				@foreach($header_slides as $slide)
				<div class="page-header-full__slide">
					<div
						class="page-header-full__slide-desktop"
						@if ($slide['media_type'] == 'Image')
						style="background: url({!! $slide['desktop_background_image']['url'] !!}) no-repeat center; background-size: cover;"
						@endif
					>
						@if ($slide['media_type'] == 'Video')
						<video src="{!! $slide['desktop_video'] !!}" autoplay muted loop playsinline></video>
						@endif
						<img src="{!! $slide['desktop_content_image']['url'] !!}" alt="">

						@if($slide['popup_video_link'] && $slide['video_link_text'])
							<p>
								<br>
								<a class="content-button content-button--block video-trigger" data-lity data-no-swup href=" {!! $slide['video_link'] !!} ">
									{!! $slide['video_link_text'] !!}
								</a>
							</p>
						@endif

					</div>
					<div
						class="page-header-full__slide-mobile"
						@if ($slide['media_type'] == 'Image')
						style="background: url({!! $slide['mobile_background_image']['url'] !!}) no-repeat center; background-size: cover;"
						@endif
					>
						@if ($slide['media_type'] == 'Video')
						<video src="{!! $slide['mobile_video'] !!}" autoplay muted loop playsinline></video>
						@endif
						<img src="{!! $slide['mobile_content_image']['url'] !!}" alt="">

						@if($slide['popup_video_link'] && $slide['video_link_text'])
							<p>
								<br>
								<a class="content-button content-button--block video-trigger" data-no-swup href=" {!! $slide['video_link'] !!} " target="_blank">
									{!! $slide['video_link_text'] !!}
								</a>
							</p>
						@endif

					</div>
				</div>
				@endforeach

			</div>

		</div>

	</div>
	@endif
@endif
@endif
