<div id="footer-cta-wrapper">
@if($show_footer_call_to_action && $footer_cta)
<div class="section section-footer section--orange">

	<div class="inner-wrapper">

		<div class="section-footer__inner content">

			{!! $footer_cta !!}

		</div>

	</div>

</div>
@endif
</div>

<footer class="footer">

	<div class="inner-wrapper">

		<div class="footer__inner">

			<div class="footer__column--column-1 footer__column">

				<a class="footer__footer-logo" href="{!! home_url() !!}"><img src="@asset('images/logo-white.svg')" alt=""></a>

				<a class="footer__footer-logo-2" href="https://whv.org.au/" target="_blank"><img src="@asset('images/whv-logo.svg')" alt=""></a>

				<div class="footer__footer-sub"><p>shEqual is an initiative led by<br/> Women’s Health Victoria</p></div>

			</div>

			<div class="footer__column--column-2 footer__column">

				<div class="footer__main-menu">

					@if (has_nav_menu('footer_1_navigation'))
					{!! wp_nav_menu(['theme_location' => 'footer_1_navigation', 'menu_class' => 'nav']) !!}
					@endif

				</div>

				<div class="footer__sub-menu">

					@if (has_nav_menu('footer_2_navigation'))
					{!! wp_nav_menu(['theme_location' => 'footer_2_navigation', 'menu_class' => 'nav']) !!}
					@endif

				</div>

				<div class="footer__socials">

					@if($facebook_link)
					<a href="{!! $facebook_link !!}" target="_blank"><img src="@asset('images/facebook.svg')" alt=""></a>
					@endif

					@if($twitter_link)
					<a href="{!! $twitter_link !!}" target="_blank"><img src="@asset('images/twitter.svg')" alt=""></a>
					@endif

					@if($instagram_link)
					<a href="{!! $instagram_link !!}" target="_blank"><img src="@asset('images/instagram.svg')" alt=""></a>
					@endif

					@if($youtube_link)
					<a href="{!! $youtube_link !!}" target="_blank"><img src="@asset('images/youtube.svg')" alt=""></a>
					@endif

					@if($linkedin_link)
					<a href="{!! $linkedin_link !!}" target="_blank"><img src="@asset('images/linkedin.svg')" alt=""></a>
					@endif
				</div>

			</div>

			<div class="footer__column--column-3 footer__column">

				{!! $footer_content !!}

			</div>

		</div>

	</div>

</footer>
