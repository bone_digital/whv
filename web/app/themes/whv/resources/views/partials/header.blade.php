<header class="header">

	<div class="inner-wrapper">

		<div class="header__inner">

			<a class="header__logo" href="{!! home_url() !!}"><img src="@asset('images/logo.svg')" alt=""></a>

			<nav class="header__nav">

				@if (has_nav_menu('primary_navigation'))
				{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
				@endif

			</nav>

			<a href="#" data-no-swup class="menu-trigger">

				<span class='menu-trigger__bar menu-trigger__bar--bar-1'></span>
				<span class='menu-trigger__bar menu-trigger__bar--bar-2'></span>

			</a>

		</div>

	</div>

</header>

<div class="mobile-menu">

	<div class="inner-wrapper">

		<div class="mobile-menu__inner">

			@if (has_nav_menu('primary_navigation'))
			{!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
			@endif

		</div>

	</div>

</div>
