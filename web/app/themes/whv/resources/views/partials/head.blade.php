<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="https://use.typekit.net/owg6teq.css">
	{!! App::googleAnalytics() !!}
	@php wp_head() @endphp
	<script>
		function onloadCallback()
		{
			renderRecaptcha();
		}
	</script>
</head>
