<div class="section section-{!! $name !!}
{!! $section_class !!}
{!! $background_colour !!}
{!! $parallax !!}
{!! $remove_top_spacing ? 'override__top-padding-none':''; !!}
{!! $remove_bottom_spacing ? 'override__bot-padding-none':''; !!}"
data-rellax-speed="4" data-rellax-percentage="0.1">
	@if($modules)
        @foreach($modules as $module)
            @include('modules.' . $module['name'], $module['data'])
        @endforeach
	@endif
</div>
