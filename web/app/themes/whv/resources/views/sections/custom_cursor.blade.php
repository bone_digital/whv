<div class="section section-{!! $name !!}" style="background: {!! $background_colour !!}">

	<div class="inner-wrapper">

		@if($images)
		<div class='custom-cursor'
			 data-images="@foreach($images as $image){!! $image['url'] !!},@endforeach"
		>
			<canvas id="custom-cursor__canvas">

			</canvas>
		</div>
		@endif

		<div class="cursor-text">

				@if($content)
				<div class="cursor-text__content content"><h2>{!! $content !!}</h2></div>
				@endif

				@if($link)
				<a class="content-button content-button--block cursor-button-mobile" href="{!! $link['url'] !!}">{!! $link['title'] !!}</a>
				@endif

		</div>

	</div>

</div>
