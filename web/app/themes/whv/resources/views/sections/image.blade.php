<div class="section section-{!! $name !!}" style="background: {!! $background_colour !!}">

	<div class="inner-wrapper">

		<div class='section-image__inner'>

			@if($image)
				{!! App::generateImgTag($image, 'full') !!}
			@endif

		</div>

	</div>

</div>
