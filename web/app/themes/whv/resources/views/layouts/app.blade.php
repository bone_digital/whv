<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
	  <!-- Facebook Pixel Code -->
	  <script>
		  !function(f,b,e,v,n,t,s)
		  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
			  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
			  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
			  n.queue=[];t=b.createElement(e);t.async=!0;
			  t.src=v;s=b.getElementsByTagName(e)[0];
			  s.parentNode.insertBefore(t,s)}(window, document,'script',
			  'https://connect.facebook.net/en_US/fbevents.js');
		  fbq('init', '365807604684106');
		  fbq('track', 'PageView');
	  </script>
	  <noscript><img height="1" width="1" style="display:none"
					 src="https://www.facebook.com/tr?id=365807604684106&ev=PageView&noscript=1"
		  /></noscript>
	  <!-- End Facebook Pixel Code -->

  @php do_action('get_header') @endphp
	@include('partials.header')

	@if($show_equals)
	<div class="site-equal"></div>
	@endif

    <div id="body-wrapper" class="body-wrapper">
		@include('partials.page-header')

		  @yield('content')
	</div>

    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
  </body>
</html>
