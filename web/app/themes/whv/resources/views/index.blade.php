@extends('layouts.app')

@section('content')

	@if($featured_posts and $is_home)
	<div class="featured-posts">

		<div class='inner-wrapper'>

			<div class="featured-posts__inner">

				@foreach($featured_posts as $post)
				@include('cards.card_blog-featured', $post)
				@endforeach

			</div>

		</div>

	</div>
	@endif

	@if($posts)
	<div class="blog-posts">

		<div class='inner-wrapper'>

			<div class="blog-posts__inner">

				@foreach($posts as $post)
				@include('cards.card_blog', $post)
				@endforeach

				@if($remainder)
					@for($i=0; $i<=$remainder; $i++)
						<div class="empty-div card-blog"></div>
					@endfor
				@endif

			</div>

			@if($show_load_more)
			<div class="blog-posts__load-more-wrap">

				<a data-no-swup href="#" class="content-button content-button--underlined blog-posts__load-more">Load more</a>
				<input type="hidden" value="{!! $max_pages !!}" class="max_num_pages">
				<input type="hidden" value="1" class="current_page">

			</div>
			@endif

		</div>

	</div>
	@endif

@endsection
