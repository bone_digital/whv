@extends('layouts.app')

@section('content')

<div class="single-post-wrapper">

	<div class="inner-wrapper">

		<div class="single-post-wrapper__inner">

			<div class="single-post-wrapper__back">

				<a href="{!! $back_url !!}">Back to Explore</a>

			</div>

			<div class="single-post-wrapper__top">

				@if($category)
				<div class="single-post-wrapper__category single-post-wrapper__left-width">{!! $category !!}</div>
				@endif

				<div class="single-post-wrapper__title single-post-wrapper__right-width"><h2>{!! $title !!}</h2></div>

			</div>

			<div class="single-post-wrapper__middle">

				<div class="single-post-wrapper__sub-share single-post-wrapper__left-width">

					@if($sub_title)
					<div class="single-post-wrapper__sub-title"><p>{!! $sub_title !!}</p></div>
					@endif

					<div class="single-post-wrapper__share">

						<span>Share</span>
						<div class="single-post-wrapper__share-icons">

							<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={!! $post_link !!}"><img src="@asset('images/facebook-orange.svg')" alt=""></a>
							<a target="_blank" href="https://twitter.com/intent/tweet?text={!! $post_link !!}"><img src="@asset('images/twitter-orange.svg')" alt=""></a>
							<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={!! $post_link !!}&title=&summary=&source="><img src="@asset('images/linkedin-orange.svg')" alt=""></a>

						</div>

					</div>

				</div>

				@if($featured_image)
				<div class="single-post-wrapper__featured-image single-post-wrapper__right-width" style="background: url({!! $featured_image !!}) no-repeat center; background-size: cover;"></div>
				@endif

			</div>

			<div class="single-post-wrapper__bottom">

				<div class="single-post-wrapper__link-wrap single-post-wrapper__left-width">

					@if($full_article_link)
					<p>Read the full article</p>

					<a class="single-post-wrapper__link" href="{!! $full_article_link !!}" target="_blank">{!! $full_article_title !!}</a>
					@endif

					@if($supporting_link)
					<a class="single-post-wrapper__supporting-link" href="{!! $supporting_link !!}}">{!! $supporting_link_title !!}</a>
					@endif

				</div>

				<div class="single-post-wrapper__content content single-post-wrapper__right-width">

					@php
					the_content()
					@endphp

				</div>

			</div>

		</div>

	</div>

</div>

@endsection
