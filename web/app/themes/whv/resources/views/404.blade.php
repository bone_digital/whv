@extends('layouts.app')

@section('content')
<div class="error-wrapper">

	<div class="inner-wrapper">

		<div class="error-wrapper__inner">

			<h1>404</h1>
			<h2>Page Not Found</h2>
			<a class="content-button content-button--block" href="{!! home_url() !!}">Back to Home</a>

		</div>

	</div>

</div>
@endsection
