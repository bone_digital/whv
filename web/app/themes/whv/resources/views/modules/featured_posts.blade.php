<div class="module module-posts" data-wp>

		<div class='module-posts__inner'>

			@if($post_items)
			<div class='module-posts__posts'>
				@php($post_count = 0)
				@foreach($post_items as $post)
					@php($post_count++)
				<a href="{!! $post['link'] !!}" class="module-posts__post">

					<div class="module-posts__post-inner">

						<div class='module-posts__post-top'>

							<div class='module-posts__category'>{!! $post['category'] !!}</div>

							<h2>{!! $post['title'] !!}</h2>

						</div>

						@if($post['sub_title'])
						<div class="module-posts__post-bottom">
							<p>{!! $post['sub_title'] !!}</p>
						</div>
						@endif

					</div>

				</a>

				@if($post_count == 1)
				<div class="module-posts__image-mobile">

					<div class="module-posts__image-inner" style="background: url({!! $image['url'] !!}) no-repeat center; background-size: cover;"></div>

				</div>
				@endif
				@endforeach
			</div>
			@endif

			<div class="module-posts__image">

				<div class="module-posts__image-inner" style="background: url({!! $image['url'] !!}) no-repeat center; background-size: cover;"></div>

			</div>

		</div>

</div>
