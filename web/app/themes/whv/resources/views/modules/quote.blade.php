<div class="module module-quote" data-wp>

	<div class="inner-wrapper">

		<div class="module-quote__inner">

			<div class="module-quote__quote">

				<div class="module-quote__quote-block-left"></div>


				<div class="module-quote__quote-inner">

					<h2>{!! $quote !!}</h2>

					<div class="module-quote__attribution">

					{!! $attribution !!}

					</div>

				</div>

				<div class="module-quote__quote-block-right"></div>

			</div>

			<div class="module-quote__image" style="background: url({!! $image['url'] !!}) no-repeat center; background-size: cover;">

				<div class="module-quote__image-block-left"></div>
				<div class="module-quote__image-block-right"></div>

			</div>

		</div>

	</div>

</div>
