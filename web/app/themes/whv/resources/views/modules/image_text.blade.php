<div class="module module-image-text {!! $module_layout !!}" data-wp>

	<div class="inner-wrapper">

		<div class='module-image-text__inner'>

			@if($content)
			<div class="module-image-text__content content">
				<div class='module-image-text__text'>
					{!! $content !!}
				</div>
			</div>
			@endif

			@if($image)
				<div class="module-image-text__image" style="background: url({!! $image['url'] !!}) no-repeat center; background-size: cover;">

					<div class="module-image-text__square-left"></div>
					<div class="module-image-text__square-right"></div>

				</div>
			@endif
		</div>

	</div>

</div>
