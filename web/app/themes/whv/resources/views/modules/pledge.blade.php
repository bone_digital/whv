<div class="module module-pledge" data-wp>

	<div class="inner-wrapper">

		<div class='module-pledge__inner'>

			@if($pledge_image)
			<a
				data-vars-ga-category="Download Image"
				data-vars-ga-action="Click"
				data-vars-ga-label="Download Image"
				target="_blank"
				download="pledge.jpg"
				href="{!! $pledge_image['url'] !!}"
				class="module-pledge__pledge module-pledge__column">

				<img src="{!! $pledge_image['url'] !!}" alt="">

				<h5>Download image</h5>

			</a>
			@endif

			@if($content)
			<div class="module-pledge__column content">

				{!! $content !!}

			</div>
			@endif

			<div class="module-pledge__share">

				<span>Share</span>
				<div class="module-pledge__share-icons">

					<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u={!! $pledge_image['url'] !!}"><img src="@asset('images/facebook-orange.svg')" alt=""></a>
					<a target="_blank" href="https://twitter.com/intent/tweet?text={!! $pledge_image['url'] !!}"><img src="@asset('images/twitter-orange.svg')" alt=""></a>
					<a target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url={!! $pledge_image['url'] !!}&title=&summary=&source="><img src="@asset('images/linkedin-orange.svg')" alt=""></a>

				</div>

			</div>

		</div>

	</div>

</div>
