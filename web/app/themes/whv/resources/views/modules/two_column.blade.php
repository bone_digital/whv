<div class="module module-two-column" data-wp>

	<div class="inner-wrapper">

		<div class='module-two-column__inner'>

			@if($column_1)
			<div class="module-two-column__column content">

				{!! $column_1 !!}

			</div>
			@endif

			@if($column_2)
			<div class="module-two-column__column content">

				{!! $column_2 !!}

			</div>
			@endif

		</div>

	</div>

</div>
