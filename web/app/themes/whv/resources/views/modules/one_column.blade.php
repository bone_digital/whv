<div class="module module-{!! $name !!} {!! $width !!}" data-wp>

	<div class="inner-wrapper">

		<div class='module-one_column__inner'>

			@if($content)
			<div class="module-one_column__column content">

				{!! $content !!}

			</div>
			@endif

		</div>

	</div>

</div>
