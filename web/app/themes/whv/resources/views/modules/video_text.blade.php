<div class="module module-video-text {!! $module_layout !!}" data-wp>

	<div class="inner-wrapper">

		<div class='module-video-text__inner'>

			@if($content)
			<div class="module-video-text__content content">
				{!! $content !!}
			</div>
			@endif

			@if($video_link)
				<div class="module-video-text__video">

					<a data-lity style="background: url({!! $video_thumbnail['url'] !!}) no-repeat center; background-size: cover;" href="{!! $video_link !!}"></a>

				</div>
			@endif
		</div>

	</div>

</div>
