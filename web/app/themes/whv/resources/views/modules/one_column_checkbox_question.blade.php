<div class="module module-{!! $name !!} {!! $width !!}" data-wp>

	<div class="inner-wrapper">

		<div class='module-one_column_question__inner'>

			@if($question)
			<?php $elem_id = uniqid(); ?>
			<div class="module-one_column_question__column content question">
				<input class="content-question" type="checkbox" id="{!! $elem_id !!}" />
				<label for="{!! $elem_id !!}">{!! $question !!}</label>
			</div>
			@endif

			@if($learn_more_content)
			<div class="module-one_column_question__column content load-more-content">
				{!! $learn_more_content !!}
			</div>
			@endif

		</div>
	</div>
</div>
