<div class="module module-three-column" data-wp>

	<div class="inner-wrapper">

		<div class='module-three-column__inner'>

			@if($column_1)
			<div class="module-three-column__column content">

				{!! $column_1 !!}

			</div>
			@endif

			@if($column_2)
			<div class="module-three-column__column content">

				{!! $column_2 !!}

			</div>
			@endif

			@if($column_3)
			<div class="module-three-column__column content">

				{!! $column_3 !!}

			</div>
			@endif

		</div>

	</div>

</div>
