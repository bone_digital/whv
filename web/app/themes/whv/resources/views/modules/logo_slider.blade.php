<div class="module module-logo" data-wp>

	<div class="inner-wrapper">

		<div class="module-logo__inner">

			<div class="module-logo__title"><h1>{!! $module_title !!}</h1></div>

			<div class="module-logo__slider owl-carousel">

				@foreach($logos as $logo)
				<div class="module-logo__slide">

					<div class="module-logo__slide-inner">

						<img src="{!! $logo['url'] !!}" alt="{!! $logo['title'] !!}">

					</div>

				</div>
				@endforeach

			</div>

		</div>

	</div>

</div>
