//Full list of composer repositories which may be useful
/******************
/* Geneeral
/*******************/
//Duplicate Post
"wpackagist-plugin/duplicate-post" : "*",

//Google Analtyics
"wpackagist-plugin/google-analytics-for-wordpress" : "*",

//Google Site Kit
"wpackagist-plugin/google-site-kit" : "*",

//Gravity Forms ACF Add On - adds in teh ability to select a form via ACF
"wpackagist-plugin/acf-gravityforms-add-on": "*"

/******************
/* Live Sites (self hosted)
/*******************/
//Wordfence
"wpackagist-plugin/wordfence" : "*",

//MailGun
"wpackagist-plugin/mailgun" : "*",

//----------------
//Alternatives
//----------------
//iThemes Secuirty
"wpackagist-plugin/better-wp-security" : "*",

//Sendgrid
"wpackagist-plugin/sendgrid-email-delivery-simplified" : "*",

/******************
/* WooCommerce / Multisite
/*******************/
//WooCommerce
"wpackagist-plugin/woocommerce" : "*",

//WooCommerce Integration for Sage 9
"roots/sage-woocommerce" : "*",

//Roots MultiSite - needed for a Bedrock setup with a MultiSite install
"roots/multisite-url-fixer" : "*",

/******************
/* SpinUpWP Servers
/*******************/
//SpinUpWP - Modified by Bone to remove SpinUp branding and make cache buttons clearer
"wpackagist-plugin/spinupwp" : "*",

/******************
/* Kinsta Servers
/*******************/
//Repositry
{
  "type": "package",
  "package": {
    "name": "kinsta/kinsta-mu-plugins",
    "type": "wordpress-muplugin",
    "version": "2.0.15",
    "dist": {
      "url": "https://kinsta.com/kinsta-tools/kinsta-mu-plugins.zip",
      "type": "zip"
    }
  }
}

//require
kinsta/kinsta-mu-plugins: "*",

