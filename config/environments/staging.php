<?php
/**
 * Configuration overrides for WP_ENV === 'staging'
 */

use Roots\WPConfig\Config;

/**
 * You should try to keep staging as close to production as possible. However,
 * should you need to, you can always override production configuration values
 * with `Config::define`.
 *
 * Example: `Config::define('WP_DEBUG', true);`
 * Example: `Config::define('DISALLOW_FILE_MODS', false);`
 */

/**
 * Disable Kinsta cache on staging
 */
if ( !defined('KINSTA_DEV_ENV') )
{
	define('KINSTA_DEV_ENV', true);
}
if ( !defined('JETPACK_STAGING_MODE') )
{
	define('JETPACK_STAGING_MODE', true);
}
